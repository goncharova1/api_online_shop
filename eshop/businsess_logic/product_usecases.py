from typing import Optional, List

from eshop.businsess_logic.product import Product
import uuid
from eshop.businsess_logic.order import Order
#from eshop.data_access.order_repo import get_by_id, get_many
from eshop.data_access.product_repo import get_by_id, save, get_many


def product_create(id: str, name, price) -> Product:
    product = Product(
        id=id,
        name=name,
        price=price,
    )
    save(product)

    return product




def product_get_by_id(id: str) -> Optional[Product]:
    return get_by_id(id)


def product_get_many(page: int, limit: int) -> List[Product]:
    return get_many(page=page, limit=limit)
