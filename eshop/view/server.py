from flask import Flask, request, jsonify
from marshmallow import ValidationError

from eshop.businsess_logic.order_usecases import order_create, \
    order_get_many, order_get_by_id
from eshop.businsess_logic.product_usecases import product_create, \
    product_get_by_id, product_get_many
from eshop.view.order_schemas import OrderCreateDtoSchema, \
    OrderSchema, OrderGetManyParams
from eshop.view.product_schemas import ProductGetManyParams, ProductSchema

app = Flask(__name__)


@app.post("/api/v1/order")
def order_create_endpoint():
    """
    Конечная точка для создания заказа.

    Тело запроса должно содержать JSON-объект со следующей структурой:
    {
        "product_ids": [список идентификаторов продуктов]
    }

    Возвращает созданный заказ в виде JSON-объекта.
    """
    try:
        order_create_dto = OrderCreateDtoSchema().load(request.json)
    except ValidationError as err:
        return err.messages, 400

    try:
        order = order_create(
            product_ids=order_create_dto['product_ids']
        )
    except Exception as e:
        return {
            "error": str(e)
        }

    return OrderSchema().dump(order)


@app.get("/api/v1/order")
def order_get_many_endpoint():
    """
    Конечная точка для получения нескольких заказов.

    Параметры запроса:
    - page: номер страницы (по умолчанию: 1)
    - limit: количество заказов на странице (по умолчанию: 10)

    Возвращает список заказов в виде JSON-объекта.
    """
    try:
        order_get_many_params = OrderGetManyParams().load(request.args)
    except ValidationError as err:
        return err.messages, 400

    order = order_get_many(
        page=order_get_many_params['page'],
        limit=order_get_many_params['limit'],
    )

    return OrderSchema(many=True).dump(order)


@app.get("/api/v1/order/<id>")
def order_get_by_id_endpoint(id):
    """
    Конечная точка для получения одного заказа по его идентификатору.

    Возвращает заказ в виде JSON-объекта.
    Если заказ не найден, возвращает ошибку 404.
    """
    order = order_get_by_id(id)

    if order is None:
        return {
            "error": 'Не найдено'
        }, 404

    return OrderSchema().dump(order)


@app.post("/api/v1/product")
def order_create_product():
    """
    Конечная точка для создания продукта.

    Тело запроса должно содержать JSON-объект со следующей структурой:
    {
        "id": идентификатор продукта,
        "name": название продукта,
        "price": цена продукта
    }

    Возвращает созданный продукт в виде JSON-объекта.
    """
    try:
        product_create_dto = ProductSchema().load(request.json)
    except ValidationError as err:
        return err.messages, 400

    try:
        product = product_create(
            id=product_create_dto['id'],
            name=product_create_dto['name'],
            price=product_create_dto['price']
        )
    except Exception as e:
        return {
            "error": str(e)
        }

    return ProductSchema().dump(product)


@app.get("/api/v1/product/<id>")
def product_get_by_id_endpoint(id):
    """
    Конечная точка для получения одного продукта по его идентификатору.

    Возвращает продукт в виде JSON-объекта.
    Если продукт не найден, возвращает ошибку 404.
    """
    product = product_get_by_id(id)

    if product is None:
        return {
            "error": 'Не найдено'
        }, 404

    return ProductSchema().dump(product)


@app.get("/api/v1/product")
def product_get_many_endpoint():
    """
    Конечная точка для получения нескольких продуктов.

    Параметры запроса:
    - page: номер страницы (по умолчанию: 1)
    - limit: количество продуктов на странице (по умолчанию: 10)

    Возвращает список продуктов в виде JSON-объекта.
    """
    try:
        product_get_many_params = ProductGetManyParams().load(request.args)
    except ValidationError as err:
        return err.messages, 400

    product = product_get_many(
        page=product_get_many_params['page'],
        limit=product_get_many_params['limit']
    )

    return ProductSchema(many=True).dump(product)


def run_server():
    """
    Запускает сервер Flask.
    """
    app.run()